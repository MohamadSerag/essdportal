from pathlib import Path


app_path = Path(__file__).parent
rvw_dir_path = app_path.joinpath('static/reviews')
pds_man_dir_path = app_path.joinpath('static/pdfs/manuals/pds')
sp3d_man_dir_path = app_path.joinpath('static/pdfs/manuals/sp3d')
rvw_ext = '.exe'
dsc_ext = '.csv'
pdf_ext = '.pdf'




