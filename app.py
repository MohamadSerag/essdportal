import os
from contextlib import suppress
from time import ctime
from pathlib import Path
import math

import flask as fl
import cfg

# Sessions handling ################################################################################


class ReviewItem:
    def __init__(self, name, desc=''):
        self.full_name = Path(name)
        self.name = self.full_name.stem    # reomves ext and parent folder if review folder
        self.path = cfg.rvw_dir_path.joinpath(self.full_name)
        self.desc = desc
        self.mtime = os.stat(str(self.path)).st_mtime
        self.mtime_str = ctime(self.mtime)

    def is_folder(self):
        return isinstance(self, ReviewFolder)


class ReviewSession(ReviewItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.size = round(self.path.stat().st_size / 1024**2, 1)


class ReviewFolder(ReviewItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.size = round(math.fsum(f.stat().st_size for f in self.path.iterdir()) / 1024**2, 1)

    @property
    def children(self):
        path = cfg.rvw_dir_path.joinpath(self.full_name)
        reviews = [ReviewSession(self.full_name.joinpath(p.name)) for p in path.iterdir()]
        return assign_desc(reviews)


def assign_desc(reviews, file='desc.csv'):
    with cfg.rvw_dir_path.joinpath(file).open() as desc_file:
        ls = desc_file.read().splitlines()

    d = {s.split(',')[0].strip().lower(): s.split(',')[1].strip() for s in ls}
    for r in reviews:
        r.desc = d.get(str(r.name).lower(), '')

    return reviews


def make_obj(name):
    if cfg.rvw_dir_path.joinpath(name).is_dir():
        return ReviewFolder(name)
    return ReviewSession(name)


def create_review_items(col, order):
    reviews = [make_obj(path.name) for path in cfg.rvw_dir_path.iterdir() if path.suffix != cfg.dsc_ext]
    reviews = assign_desc(reviews)
    reverse = False if order == 'u' else True

    if col == 1:
        reviews.sort(key=lambda rvw_sess: str(rvw_sess.name)[:4], reverse=reverse)

    elif col == 2:
        reviews.sort(key=lambda rvw_sess: rvw_sess.desc, reverse=reverse)

    elif col == 3:
        reviews.sort(key=lambda rvw_sess: rvw_sess.size, reverse=reverse)

    elif col == 4:
        reviews.sort(key=lambda rvw_sess: rvw_sess.mtime, reverse=reverse)

    return reviews


def get_man_names(system):
    man_dir_path = cfg.pds_man_dir_path if system == 'pds' else cfg.sp3d_man_dir_path

    return [{'name': name, 'name_noext': name.rstrip(str(cfg.pdf_ext))}
            for name in os.listdir(str(man_dir_path))]

# Views ############################################################################################

app = fl.Flask(__name__)


@app.route('/')
def home():
    # try:
    return fl.render_template('navis.html', items=create_review_items(4, 1), col=4)
    # except Exception as err:
    #     return str(err)


@app.route('/<int:col>/<order>')
def sort(col, order):
    return fl.render_template('navis.html', items=create_review_items(col, order), col=col)


@app.route('/network/')
def network():
    return fl.render_template('network.html')


@app.route('/contacts/')
def contacts():
    return fl.render_template('contacts.html')


@app.route('/about/')
def about():
    return fl.render_template('about.html')


@app.route('/manuals/<system>')
def man_pd(system):
    return fl.render_template('manuals.html',
                              mans=get_man_names(system),
                              s=system)


@app.route('/static/pdf/manuals/<system>/<filename>')
def get_pdf(system, filename):
    return fl.send_from_directory(str(cfg.app_path.joinpath('static/pdfs/manuals', system)),
                                  filename=filename)


if __name__ == '__main__':
    app.run(debug=True)
